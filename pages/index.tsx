import commerce from '@lib/api/commerce'
import { Layout } from '@components/common'
import { ProductCard } from '@components/product'
import { Grid, Button } from '@components/ui'

import Link from 'next/link'
import Image from 'next/image'

import s from './Index.module.css'

import type { GetStaticPropsContext, InferGetStaticPropsType } from 'next'

export async function getStaticProps({
  preview,
  locale,
  locales,
}: GetStaticPropsContext) {
  const config = { locale, locales }
  const { products } = await commerce.getAllProducts({
    variables: { first: 12 },
    config,
    preview,
  })
  const { categories, brands } = await commerce.getSiteInfo({ config, preview })
  const { pages } = await commerce.getAllPages({ config, preview })

  return {
    props: {
      products,
      categories,
      brands,
      pages,
    },
    revalidate: 14400,
  }
}

export default function Home({
  products,
}: InferGetStaticPropsType<typeof getStaticProps>) {
  return (
    <>
      <section className="grid grid-cols-1 md:grid-cols-12 mb-8">
        <div className="relative md:col-span-7 h-52 md:h-full">
          <Image
            src="/barkriverimage.jpeg"
            alt="Bark River Knives banner"
            layout="fill"
          />
        </div>
        <div className="px-6 mt-6 md:col-span-5 md:pl-8 md:pb-16">
          <h1 className="text-4xl md:pt-24 md:text-6xl uppercase">
            <strong className="block md:mb-2">Bark River</strong>
            Knives
          </h1>
          <h3 className="text-xl mb-6 uppercase md:pt-8 md:mb-16 md:text-2xl">
            Over 4,000 knives in stock!
          </h3>
          <Button variant="slim" type="submit">
            Sign Up For Our Newsletter
          </Button>
        </div>
      </section>
      <section className="hidden md:block grid grid-cols-1 mb-8">
        <Link href="/search">
          <Image
            src="/hinderersection.jpeg"
            alt="Hinder Knives banner"
            width="1440"
            height="600"
          />
        </Link>
      </section>
      <section
        className={
          s.bannerMobile + ' grid grid-cols-1 md:grid-cols-12 mb-8 md:hidden'
        }
      >
        <div className="flex relative md:col-span-7 h-52 md:h-full items-center justify-center">
          <Image
            src="/hinderer-mob.png"
            alt="Bark River Knives banner"
            width="164"
            height="107"
          />
        </div>
        <div className={' px-6 mt-6 mb-12 md:col-span-5 md:pl-8 md:pb-16'}>
          <h1 className="text-4xl mb-6 md:pt-24 md:text-6xl uppercase">
            <strong className="md:mb-2">Hinderer</strong>
            Knives
          </h1>
          <p className="text-l mb-4">
            Rick Hinderer started building high quality art knives over 28 years
            ago. In that time, he has risen to the top of the pantheon of great
            knife designers.
          </p>
          <Button variant="slim" type="submit">
            Shop Hinderer
          </Button>
        </div>
      </section>
      <Grid>
        {products.slice(0, 3).map((product, i) => (
          <ProductCard
            key={product.id}
            product={product}
            imgProps={{
              width: i === 0 ? 1080 : 540,
              height: i === 0 ? 1080 : 540,
            }}
          />
        ))}
      </Grid>
    </>
  )
}

Home.Layout = Layout
