import { FC, useState } from 'react'
import cn from 'classnames'

import Link from 'next/link'
import Image from 'next/image'

import { useRouter } from 'next/router'
import { useUI } from '@components/ui/context'
import type { Page } from '@commerce/types/page'
import getSlug from '@lib/get-slug'
import { Container, Button, Input } from '@components/ui'

import s from './Footer.module.css'

interface Props {
  className?: string
  children?: any
  pages?: Page[]
}

const LEGAL_PAGES = ['terms-of-use', 'shipping-returns', 'privacy-policy']

const Footer: FC<Props> = ({ className, pages }) => {
  const { sitePages, legalPages } = usePages(pages)
  const rootClassName = cn(className)

  const { setModalView, closeModal, openModal } = useUI()

  const [email, setEmail] = useState('')
  const [loading, setLoading] = useState(false)
  const [disabled, setDisabled] = useState(false)

  const handleSubmit = async (e: React.SyntheticEvent<EventTarget>) => {
    e.preventDefault()
  }

  const handleCreateAccount = (e: React.SyntheticEvent<EventTarget>) => {
    e.preventDefault()
    openModal()
    setModalView('SIGNUP_VIEW')
  }

  const handleSignIn = (e: React.SyntheticEvent<EventTarget>) => {
    e.preventDefault()
    openModal()
    setModalView('LOGIN_VIEW')
  }

  return (
    <footer className={s.root}>
      <Container>
        <div className="grid grid-cols-1 lg:grid-cols-12 border-b border-accents-2 text-secondary bg-primary transition-colors duration-150">
          <div className={s.footerBrand + ' col-span-1 lg:col-span-3'}>
            <Link href="/">
              <Image
                src="/footer-logo.png"
                alt="DLT Trading Logo"
                width="136"
                height="67"
              />
            </Link>
            <a href="tel:8776222397" className={s.footerPhone}>
              877-622-2397
            </a>
            <p className={s.footerOpenHours}>Mon-Fri, 8-5 CT</p>
          </div>
          <div
            className={
              s.footerLinks +
              ' col-span-1 lg:col-span-9 grid grid-cols-1 lg:grid-cols-12'
            }
          >
            <div className="col-span-1 lg:col-span-3">
              <h3 className={s.footerHeading}>My Account</h3>
              <ul className="flex flex-initial flex-col md:flex-1">
                <li className="py-3 md:py-0 md:pb-4">
                  <Link href="/orders">
                    <a className="text- hover:text-accents-6 transition ease-in-out duration-150">
                      Order Status
                    </a>
                  </Link>
                </li>
                <li className="py-3 md:py-0 md:pb-4">
                  <Link href="/wishlist">
                    <a className="text-secondary hover:text-accents-6 transition ease-in-out duration-150">
                      Wishlist
                    </a>
                  </Link>
                </li>
                <li
                  onClick={(e) => handleSignIn(e)}
                  className="py-3 md:py-0 md:pb-4"
                >
                  <a className="text-secondary hover:text-accents-6 transition ease-in-out duration-150">
                    Sign In
                  </a>
                </li>
                <li className="py-3 md:py-0 md:pb-4">
                  <a
                    onClick={(e) => handleCreateAccount(e)}
                    className="text-secondary hover:text-accents-6 transition ease-in-out duration-150"
                  >
                    Create an Account
                  </a>
                </li>
              </ul>
            </div>
            <div className="col-span-1 lg:col-span-5">
              <h3 className={s.footerHeading}>Help Desk</h3>
              <ul className="flex flex-initial flex-col md:flex-1">
                <li className="py-3 md:py-0 md:pb-4">
                  <Link href="/">
                    <a className="text- hover:text-accents-6 transition ease-in-out duration-150">
                      Contact Us
                    </a>
                  </Link>
                </li>
                <li className="py-3 md:py-0 md:pb-4">
                  <Link href="/customer-service">
                    <a className="text-secondary hover:text-accents-6 transition ease-in-out duration-150">
                      Customer Service
                    </a>
                  </Link>
                </li>
                <li className="py-3 md:py-0 md:pb-4">
                  <Link href="about-dlt-trading">
                    <a className="text-secondary hover:text-accents-6 transition ease-in-out duration-150">
                      About Us
                    </a>
                  </Link>
                </li>
                <li className="py-3 md:py-0 md:pb-4">
                  <Link href="https://blog.dlttrading.com">
                    <a className="text-secondary hover:text-accents-6 transition ease-in-out duration-150">
                      DLT Blog
                    </a>
                  </Link>
                </li>
              </ul>
            </div>
            <div className="col-span-1 lg:col-span-4">
              <span className={s.footerNewsletterText}>
                Receive the Latest News, Special Offers & More
              </span>
              <form
                onSubmit={handleSubmit}
                className="w-80 flex flex-col justify-between p-3"
              >
                <div className="flex flex-col space-y-3">
                  <Input type="email" placeholder="Email" onChange={setEmail} />
                  <Button
                    variant="slim"
                    type="submit"
                    loading={loading}
                    disabled={disabled}
                  >
                    Sign Up For Our Newsletter
                  </Button>
                </div>
              </form>
              <span className={s.footerNewsletterText}>
                Copyright © 2021 DLT Trading. All Rights Reserved.
              </span>
            </div>
          </div>
        </div>
      </Container>
    </footer>
  )
}

function usePages(pages?: Page[]) {
  const { locale } = useRouter()
  const sitePages: Page[] = []
  const legalPages: Page[] = []

  if (pages) {
    pages.forEach((page) => {
      const slug = page.url && getSlug(page.url)

      if (!slug) return
      if (locale && !slug.startsWith(`${locale}/`)) return

      if (isLegalPage(slug, locale)) {
        legalPages.push(page)
      } else {
        sitePages.push(page)
      }
    })
  }

  return {
    sitePages: sitePages.sort(bySortOrder),
    legalPages: legalPages.sort(bySortOrder),
  }
}

const isLegalPage = (slug: string, locale?: string) =>
  locale
    ? LEGAL_PAGES.some((p) => `${locale}/${p}` === slug)
    : LEGAL_PAGES.includes(slug)

// Sort pages by the sort order assigned in the BC dashboard
function bySortOrder(a: Page, b: Page) {
  return (a.sort_order ?? 0) - (b.sort_order ?? 0)
}

export default Footer
