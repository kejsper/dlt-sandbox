import { FC } from 'react'
import Link from 'next/link'
import { Container } from '@components/ui'
import { Searchbar, UserNav } from '@components/common'
import NavbarRoot from './NavbarRoot'

import { useUI } from '@components/ui/context'
import Image from 'next/image'
import s from './Navbar.module.css'

interface Link {
  href: string
  label: string
}
interface NavbarProps {
  links?: Link[]
}

const Navbar: FC<NavbarProps> = ({ links }) => {
  const { setModalView, openModal } = useUI()

  const onSignIn = (e: React.SyntheticEvent<EventTarget>) => {
    e.preventDefault()
    openModal()
    setModalView('LOGIN_VIEW')
  }
  return (
    <NavbarRoot>
      <Container>
        <div className={s.topBar}>
          <Container className="w-full">
            <ul className={s.topBarItems}>
              <li className={s.topBarItem}>
                <strong>FREE SHIPPING</strong> on Orders over $99
              </li>
              <li className="hidden md:block">
                <a href="tel:8776222397" className={s.phone}>
                  (877) 622-2397
                </a>
                Mon-Fri, 8-5 CT
              </li>
              <li className="hidden lg:block">
                <ul className={s.topBarSubItems}>
                  <li className={s.topBarSubItem}>
                    <Link href="/">Contact</Link>
                  </li>
                  <li className={s.topBarSubItem}>
                    <Link href="/customer-service">Customer Service</Link>
                  </li>
                  <li className={s.topBarSubItem}>
                    <Link href="/about-dlt-trading">The DLT Difference</Link>
                  </li>
                  <li className={s.topBarSubItem}>
                    <a onClick={onSignIn} className="cursor-pointer">
                      SIGN IN
                    </a>
                  </li>
                </ul>
              </li>
            </ul>
          </Container>
        </div>
        <div className="relative flex flex-row justify-between pb-4 pt-12 align-center md:pb-2 md:pt-12 mb-5">
          <div className="flex items-center flex-auto">
            <Link href="/">
              <a className={s.logo} aria-label="Logo">
                <div className="hidden md:block">
                  <Image
                    src="/logo.png"
                    alt="DLT Trading Logo"
                    width="190"
                    height="110"
                  />
                </div>
                <div className="block md:hidden">
                  <Image
                    src="/logo.png"
                    alt="DLT Trading Logo"
                    width="115"
                    height="66"
                  />
                </div>
              </a>
            </Link>
            <nav className="hidden ml-6 space-x-4 lg:block ml-56">
              <Link href="/search">
                <a className={s.link}>All</a>
              </Link>
              {links?.map((l) => (
                <Link href={l.href} key={l.href}>
                  <a className={s.link}>{l.label}</a>
                </Link>
              ))}
            </nav>
          </div>

          <div className="justify-center flex-1 hidden lg:flex">
            <Searchbar />
          </div>

          <div className="flex justify-end flex-1 space-x-8">
            <UserNav />
          </div>
        </div>

        <div className="flex pb-4 lg:px-6 lg:hidden">
          <Searchbar id="mobile-search" />
        </div>
      </Container>
    </NavbarRoot>
  )
}

export default Navbar
